require 'byebug'
# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    return 0 if self == []
    self.reduce(:+)
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array
  def square!
    self.map!{|el| el*el}
  end

  def square
    self.dup.square!
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    self.reduce([]) do |acc,el|
      acc << el if !acc.include?(el)
      acc
    end
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)
# [-9,3,2,0,9,0,-2] => [[0,4],[2,6],[3,5]]
# Will not repeat combination.
#For each element is 'arr', if there is any number from arr[i..-1] whose sum is 0, then
 # => acc << [idx,sec_idx]

class Array
  def two_sum
    #byebug
    two_sum_arr = []

    self.each_with_index do |el,idx|
      test_arr = self[idx+1..-1]
      test_arr.each_index do |sec_idx|
        two_sum_arr << [idx,sec_idx+idx+1] if el+test_arr[sec_idx] == 0
      end
    end

    two_sum_arr

  end

end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.
# First thing sort. [2,4,6,7,8,9,10] -  7/ odd 7-1/2 = 3.
# Find the index in the middle. [1,2,3,4,5,6] , 6/3 = 3, avg(3,3-1)

class Array
  def median

    return nil if self == []

    sorted_arr = self.map{|num| num.to_f}.sort
    arr_length = self.length

    case arr_length.odd?
      when true
        idx = (arr_length-1)/2
        return sorted_arr[idx]
      when false
        idx = (arr_length)/2
        return (sorted_arr[idx] + sorted_arr[idx-1])/2
      end

  end
end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2], [3, 4, 5], [6, 7, 8]
# ]
#make arr = [ [], [], [] ]
# matrix.my_transpose
#  # => [[0], [1], [2]]
# ```
#There are equally many elements as there are sub_elements in each element.
#Make a new arr with the same number of sub-arrays.
#Write as little code as possible.
# Iterate each element, and for each element in each element, plug that sub-element into
#Write as little code as possible.

# Don't use the built-in `transpose` method!

class Array
  def my_transpose

    transposed_arr = []
    self.length.times {transposed_arr<< []}

    self.each_with_index do |sub_arr,idx|
      sub_arr.each_with_index do |el,sec_idx|
        transposed_arr[sec_idx] << el #just push it in, Little things man
      end
    end

    transposed_arr

  end
end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
